#ifndef __EMPLOYEE_CPP
#define __EMPLOYEE_CPP

#include <iostream>
#include "Employee.h"
using namespace std;

namespace Records {
	Emp::Emp()
		:	mFirstName(" ")
		,	mLastName(" ")
		,	mEmpNum(-1)
		,	mSalary(kSalary_default)
		,	mHired(false)
	{
	}
	// Activity //

	void Emp::promote(int inRaiseAmount)
	{
		setSalary(getSalary() + inRaiseAmount);
	}

	void Emp::demote(int inDemeritAmount)
	{
		setSalary(getSalary() - inDemeritAmount);
	}

	void Emp::hire()
	{
		setHired(true);
	}
	void Emp::fire()
	{
		setHired(false);
	}
	void Emp::display() const
	{
		cout << "Employee: " << getFirstName() << " " << getLastName() << endl;
		cout << "--------------" << endl;
		cout << (mHired ? "Current Employee" : "Former Employee") << endl;
		cout << "Employee Number: " << getEmpNum() << endl;
		cout << "Salary: " << getSalary() << "THB" << endl;
	}

	// Setters //
	void Emp::setFirstName(string inFirstName)
	{
		mFirstName = inFirstName;
	}
	void Emp::setLastName(string inLastName)
	{
		mLastName = inLastName;
	}
	void Emp::setEmpNum(int inEmpNum)
	{
		mEmpNum = inEmpNum;
	}
	void Emp::setSalary(int inNewSalary)
	{
		mSalary = inNewSalary;
	}
	void Emp::setHired(bool inHired)
	{
		mHired = inHired;
	}

	// Accessors
	string Emp::getFirstName() const
	{
		return mFirstName;
	}
	string Emp::getLastName() const
	{
		return mLastName;
	}
	int	Emp::getEmpNum() const
	{
		return mEmpNum;
	}
	int	Emp::getSalary() const
	{
		return mSalary;
	}
	bool Emp::getHired() const
	{
		return mHired;
	}
}
#endif