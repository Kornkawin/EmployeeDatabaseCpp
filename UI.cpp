#include <iostream>
#include <stdexcept>
#include "Database.h"
using namespace std;
using namespace Records;

int displayMenu();				// Menu
void doHire(Database& inDB);	// add new employee
void doFire(Database& inDB);	// fire employee
void doPromote(Database& inDB);	// Promote

int main()
{
	Database employeeDB;
	bool done = false;
	while (!done)
	{
		int selection = displayMenu();
		switch (selection) {
		case 1:
			doHire(employeeDB);
			break;
		case 2:
			doFire(employeeDB);
			break;
		case 3:
			doPromote(employeeDB);
			break;
		case 4:
			employeeDB.displayAll();
			break;
		case 5:
			employeeDB.displayCurrent();
			break;
		case 6:
			employeeDB.displayFormer();
			break;
		case 0:
			done = true;
			break;
		default:
			cerr << "Unknown command" << endl;
		}
	}
	return 0;
}

int displayMenu()
{
	int selection;
	cout << endl;
	cout << "Employee Database" << endl;
	cout << "-----------------" << endl;
	cout << "1) Hire a new employee" << endl;
	cout << "2) Fire an employee" << endl;
	cout << "3) Promote an employee" << endl;
	cout << "4) List all employees" << endl;
	cout << "5) List all current employees" << endl;
	cout << "6) List all former employees" << endl;
	cout << "0) Quit" << endl;
	cout << endl;
	cin >> selection;
	return selection;

}
void doHire(Database& inDB)
{
	string firstName;
	string lastName;
	cout << "First name? ";
	cin >> firstName;
	cout << "Last name? ";
	cin >> lastName;
	try {
		inDB.addEmp(firstName, lastName);
	} catch (const exception&) {
		cerr << "Unable to add new employee!" << endl;
	}
}
void doFire(Database& inDB)
{
	int empNum;
	cout << "Employee Number? ";
	cin >> empNum;
	try {
		Emp& emp = inDB.getEmp(empNum);
		emp.fire();
		cout << "Employee " << empNum << " terminated." << endl;
	}
	catch (const exception&) {
		cerr << "Unable to terminate employee!" << endl;
	}
}

void doPromote(Database& inDB)
{
	int empNum;
	int raiseAmount;
	cout << "Employee Number? ";
	cin >> empNum;
	cout << "How much of a raise? ";
	cin >> raiseAmount;
	try {
		Emp& emp = inDB.getEmp(empNum);
		emp.promote(raiseAmount);
		cout << "Employee " << empNum << " promoted." << endl;
	}
	catch (const exception&) {
		cerr << "Unable to promote employee!" << endl;
	}
}