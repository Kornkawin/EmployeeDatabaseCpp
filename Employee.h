#ifndef __EMPLOYEE_H
#define __EMPLOYEE_H

#include <string>
#include <iostream>
using namespace std;

namespace Records {
	const int kSalary_default = 30000;

	class Emp
	{
	public:
		Emp();	// Default Constructor

				// Activity
		void	promote(int inRaiseAmount = 1000);	//	default param = 1000
		void	demote(int inDemeritAmount = 1000);	//	default param = 1000
		void	hire();								//Hires or rehires
		void	fire();
		void	display() const;					// Output the emp info to console

		// setters
		void	setFirstName(string inFirstName);
		void	setLastName(string inLastName);
		void	setEmpNum(int inEmpNum);
		void	setSalary(int inNewSalary);
		void	setHired(bool inHired);

		// Accessors
		string	getFirstName() const;
		string	getLastName() const;
		int		getEmpNum() const;
		int		getSalary() const;
		bool	getHired() const;

	protected:	//The accessors provide the only public way to mod or query
		string	mFirstName;
		string	mLastName;
		int		mEmpNum;
		int		mSalary;
		bool	mHired;
	};
}

#endif