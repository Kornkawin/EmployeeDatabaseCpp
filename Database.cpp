#ifndef __DATABASE_CPP
#define __DATABASE_CPP

#include <iostream>
#include <stdexcept>
#include "Database.h"
#include <string>
using namespace std;
namespace Records {
	Database::Database()
	{
		mNextEmpNum = kFirstEmpNum;
	}
	Database::~Database() {}
	Emp& Database::addEmp(string inFirstName, string inLastName)
	{
		Emp theEmployee;
		theEmployee.setFirstName(inFirstName);
		theEmployee.setLastName(inLastName);
		theEmployee.setEmpNum(mNextEmpNum++);
		theEmployee.hire();
		mEmployees.push_back(theEmployee);
		return mEmployees[mEmployees.size()-1];		// Database is updated. (Return the new whole DB that include the new employee.

	}
	Emp& Database::getEmp(int inEmpNum)
	{
		// iteration process for vector type
		for (auto iter = mEmployees.begin(); iter != mEmployees.end(); ++iter) {
			if (iter->getEmpNum() == inEmpNum)		// iter is a pointer to object
				return *iter;
		}

		// in case, not match.
		cerr << "No employee with number " << inEmpNum << endl;
		throw exception(); // end running with no return value;
	}
	Emp& Database::getEmp(string inFirstName, string inLastName)
	{
		// iteration process for vector type
		for (auto iter = mEmployees.begin(); iter != mEmployees.end(); ++iter) {
			if (iter->getFirstName()==inFirstName && iter->getLastName()==inLastName)
				return *iter;
		}

		// in case, not match.
		cerr << "No employee with this data. " << endl;
		throw exception(); // end running with no return value;

	}
	void Database::displayAll() const
	{
		for (auto iter = mEmployees.begin(); iter != mEmployees.end(); ++iter) {
			iter->display();
		}

	}
	void Database::displayCurrent() const
	{
		for (auto iter = mEmployees.begin(); iter != mEmployees.end(); ++iter) {
			if (iter->getHired()==true)
				iter->display();
		}
	}
	void Database::displayFormer() const
	{
		for (auto iter = mEmployees.begin(); iter != mEmployees.end(); ++iter) {
			if(iter->getHired()==false)
				iter->display();
		}
	}
}

#endif